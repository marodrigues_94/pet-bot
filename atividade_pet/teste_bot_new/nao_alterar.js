/**
 * Java Functions
 */

var colors = {
    reset: "\u001B[0m",
    black: "\u001B[30m",
    red: "\u001B[31m",
    green: "\u001B[32m",
    yellow: "\u001B[33m",
    blue: "\u001B[34m",
    purple: "\u001B[35m",
    cyan: "\u001B[36m",
    white: "\u001B[37m",
}

var System = Java.type("java.lang.System")
var console = System.console();

/**
 * Create a new mysql connection from injected Mysql.jar file.
 */

var mysql = Packages.Mysql;

var conn = null;
// var conn = mysql.connect('jdbc:mysql://localhost/CHATBOT?user=root&password=51518934');

// if (!conn) {
//     print(colors.red + "Erro de acesso ao database\n" + colors.reset);
// }

/**
 * END Mysql connection code.
 */


/**
 * Simulates a params from neppo-chat server
 */

var params = {
    sessionId: "1",
    fromUser: "tm2_bot",
    channel: "console_bot",
    group: "tm2_bot",
    bot: "console_bot",
    externalProtocol: "",
    externalOrderId: "",
    omniProperty: "",
    condition: "",
    page: "",
    loggedUserType: "",
    loggedUserId: "",
    httpRequest: "",
};

/**
 * Simulates a restClient connection from neppo-chat server.
 */

var restClient = {
    postForm: function (url, params) {
        response = http.post(url + "?" +  params, {}, { request: {"Content-Type": "application/x-www-form-urlencoded" }});
        return JSON.parse(response.data);
    },
    post: function (url, token, payload) {
        response = http.post(url, JSON.parse(payload), { request: {"Content-Type": "application/json", "Authorization": "Bearer " + token }});
        return response.data;
    },
    get: function (url, header) {
        response = http.get(url, header);
        return response.data;
    }
}

/**
 * Simulates a jdbc drive connection from neppo-chat server.
 */

var jdbcDao = {
    getJdbcTemplate: function () {
        return {
            queryForList: function (query) {
                var st = conn.createStatement();
                var rs = st.executeQuery(query);
                var rsmd = rs.getMetaData();
                var columnCount = rsmd.getColumnCount();
                var columnNames = [];
                var row = {};
                var result = [];

                for (var i = 1; i <= columnCount; i++) {
                    var columName = "";
                    if(rsmd.getColumnLabel(i)) {
                        columnName = rsmd.getColumnLabel(i);
                    } else {
                        columnName = rsmd.getColumnName(i);
                    }
                    columnNames.push(columnName);
                }

                while (rs.next()) {
                    for (var i = 0; i < columnCount; i++) {
                        //Add the current row's column data to list
                        row[columnNames[i]] = rs.getString(columnNames[i]);
                    }
                    result.push(row);
                }
                st.close();

                return result;
            },
            execute: function (query) {
                return this.queryForList(query)[0];
            },
            update: function (query) {
                var st = conn.prepareStatement(query);
                var rs = st.executeUpdate();
                return rs;
            }
        }
    },
}

/**
 * Function command named list
 */

var botFunctions = function (stringFunction) {
    return {
        "#TO_QUEUE": function () {
            botMessage = colors.cyan + "Redirecionado para fila (atendimento humano)" + colors.reset;
        },
        "#TO_PRIORITY_QUEUE": function () { 
            botMessage = colors.cyan + "#TO_PRIORITY_QUEUE" + colors.reset;
        },
        "#CLOSE_SESSION": function () { 
            botMessage = colors.cyan +  "#CLOSE_SESSION" + colors.reset;
        },
        "#FORWARD": function () { 
            botMessage = colors.cyan +  "#FORWARD" + colors.reset;
        },
        "#FORWARD_AGENT": function () {
            botMessage = colors.cyan +  "#FORWARD_AGENT" + colors.reset;
         },
        "#FORWARD_AGENT_WITH_MESSAGE": function () { 
            botMessage = colors.cyan +  "#FORWARD_AGENT_WITH_MESSAGE" + colors.reset;
        },
        "#SEND_AND_CLOSE": function () { 
            stringFunction.splice(0, 1);
            botMessage = colors.cyan + stringFunction.join(" ") + colors.reset;
        },
        "#SEND_MESSAGE_AND_FORWARD_TO_FINAL_AGENT": function () {
            botMessage = colors.cyan +  "#SEND_MESSAGE_AND_FORWARD_TO_FINAL_AGENT" + colors.reset;
         },
        "#SEND_MESSAGE_AND_FORWARD_TO_GROUP": function () { 
            botMessage = colors.cyan +  "#SEND_MESSAGE_AND_FORWARD_TO_GROUP" + colors.reset;
        },
        "#SEND_MESSAGE_AND_REDIRECT_TO_QUEUE": function () {
            botMessage = colors.cyan +  "#SEND_MESSAGE_AND_REDIRECT_TO_QUEUE" + colors.reset;
         },
        "#SAVE_AND_FORWARD": function () { 
            botMessage = colors.cyan +  "#SAVE_AND_FORWARD" + colors.reset;
        },
        "#CLOSE_BY_INACTIVITY": function () {
            botMessage = colors.cyan + "#CLOSE_BY_INACTIVITY" + colors.reset + colors.reset;
         },
        "#FORWARD_NODE": function () { 
            var node = stringFunction[1];
            stringFunction.splice(0, 2);
            botMessage = stringFunction.join(" ");
            return script(findNode(node));
        },
        "#SEND_MESSAGE_AND_FORWARD_AGENT": function () { 
            botMessage = colors.cyan +  "#SEND_MESSAGE_AND_FORWARD_AGENT" + colors.reset;
        },
        "#SEND_MESSAGE_AND_FORWARD_AGENT_WITH_MESSAGE": function () {
            botMessage = colors.cyan +  "#SEND_MESSAGE_AND_FORWARD_AGENT_WITH_MESSAGE" + colors.reset;
         },
    }
};

/**
 * LOGGER simulate a function from bot.
 */

var log = {
    info: function (data) {
        print(colors.green + "\n**** START LOG **** \n" + data + "\n**** END LOG ****\n\n" + colors.reset);
    }
}

/**
 *  Find a node in the nodes.js javascript file and return a javascript filename.
 * @param {string} node
 * @return string
 */

var nodes = [
    {"1": "bot/no_raiz.js"}
];  

var options = {};

var findNode = function (node) {
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i][node] != null) {
            return nodes[i][node];
        }
    }
}

/**
 * Get the function of a "#" command
 * @param {string} msg
 * @return array
 */

var getFunction = function (msg) {
    if (msg != null && msg.length > 0) {
        if (msg[0] === "#") {
            return msg.split(" ");
        }
    }
    return [];
}

/**
 * Loads a javascript code to run in the "message" function.
 * @param {string} path
 * @return string
 */

var script = function (path) {
    if (path == null) return "print('Could not find a valid javscript file); break;";
    var Paths = Java.type('java.nio.file.Paths');
    var Files = Java.type('java.nio.file.Files');
    var lines = Files.readAllLines(Paths.get(path), Java.type('java.nio.charset.StandardCharsets').UTF_8);
    var data = [];
    lines.forEach(function (line) { data.push(line); });
    return data.join("\n");
}

/**
  * Nashorn HTTP request helper
  */

 var http = {
    get: function(url, header) {
        return httpRequest(url, null, header, "GET");
    },
    post: function(url, data, header) {
        return httpRequest(url, JSON.stringify(data), header, "POST");
    }
}

function httpRequest(theUrl, data, header, method){
        
    var con = new java.net.URL(theUrl).openConnection();

    con.requestMethod = method;

    if(header != null) {
        var keys = [];
        for (var key in header.request) {
            keys.push(key);
        }
    
        for(var i = 0; i < keys.length; i++) {
            con.setRequestProperty(keys[i], header.request[keys[i]]);
        }
        
    } else {
        con.setRequestProperty("Content-Type", "application/json");
    }

    if(header.method !== "GET") {
        // Send post request
        con.doOutput=true;
        write(con.outputStream, data);
    }

    return asResponse(con);
}

function asResponse(con){
    try {
        var d = read(con.inputStream);
        return {data : d, statusCode : con.responseCode};
    } catch(err) {
        print(colors.red + err + colors.reset);
        return {data: null, statusCode: con.responseCode};
    }
}

function write(outputStream, data){
    var wr = new java.io.DataOutputStream(outputStream);
    wr.writeBytes(data);
    wr.flush();
    wr.close();
}

function read(inputStream){
    var inReader = new java.io.BufferedReader(new java.io.InputStreamReader(inputStream));
    var inputLine;
    var response = new java.lang.StringBuffer();

    while ((inputLine = inReader.readLine()) != null) {
           response.append(inputLine);
    }
    inReader.close();
    return response.toString();
}


/**
 * BOT LOOPING DIALOG FUNCIONS CODE
 */

var init = true;
var userMessage = colors.blue + "\n\nPET® Chatbot Simulator\n\n" + colors.reset;

var botMessage = "";

if (init) {
    eval(script(findNode(1))); // load a new javascript
    print(userMessage);
    userMessage = console.readLine("You:  ");
    userMessage = "init";
    print(colors.cyan + "Bot: " + colors.reset + message(userMessage) + "\n");
}

while (true) {

    userMessage = console.readLine("You:  ");
    try {
        botMessage = message(userMessage);
    } catch (err) {
        botMessage = "Error: " + err;
    }

    if (userMessage === "#sair") {
        print("Sessão cancelada com sucesso");
        break;
    }

    var stringFunction = getFunction(botMessage);
    if (stringFunction.length > 0) {
        var command = botFunctions(stringFunction)
        var executeCommand = command[stringFunction[0]]();
        if(executeCommand != undefined) {
            try {
                eval(script(findNode(1)));
                eval(executeCommand);
            } catch(err) {
                log.info("teste");
            }
        };
    }

    if(typeof botMessage == "object"){
        for(i = 0; i < botMessage.length; i++){
            print(colors.cyan + "Bot: " + colors.reset + botMessage[i] + "\n");
        }
    }
    else {
        print(colors.cyan + "Bot: " + colors.reset + botMessage + "\n");
    }
}

/**
 * END BOT LOOPING DIALOG FUNCIONS CODE
 */