/*
    - Instruções para execução da tarefa:
    1. Acessar pelo terminal da IDE a pasta teste_bot_new
    2. Executar o comando jjs nao_alterar.js
    3. Acessar o código do bot através do arquivo teste_bot_new/no_raiz.js
    4. Identificar e arrumar os erros no código
*/

var message = function(msg) {
    if(params.voltarMenu){
        if(msg == '1'){
            params.voltarMenu = false;
            params.init = false;
        }else{
            return 'Por favor, escolha uma opção válida.';
        }
    }

    if(!params.init{
        params.init = true;
        initParams();

        return 'Olá, bem vindo ao PET Neppo!'
            + params.li + 'Digite 1 para saber mais sobre a NEPPO'
            + params.li + 'Digite 2 para finalizar o atendimento';
    }

    if(msg == '1'){
        params.voltarMenu = true;
        sendWithDelayMsg('Digite 1 para voltar ao menu principal', 1500);
        return 'Em 2009, começamos a atuar em Uberlândia (MG), com a prestação de serviços de '
            + 'suporte e consultoria em TI para grandes empresas. No início trabalhávamos nos '
            + 'segmentos de telecomunicações e logística. Nesses dez anos de mercado, transformamos '
            + 'nosso modelo de negócios, aprendemos sobre diferentes segmentos, criamos produtos '
            + 'digitais específicos para atender às necessidades de nossos clientes e ampliamos nosso '
            + 'mercado alvo, passando a atender também pequenas e médias empresas. Estamos sempre em
            + 'movimento, transformando desafios complexos em soluções tecnológicas efetivas e que '
            + 'trazem ganhos de produtividade.';
    else if(msg == '2'){
        return '#CLOSE_SESSION';
    }

    return 'Por favor, digite uma opção válida.';
}

function initparams(){
    if(params.channel == 'WHATSAPP'){
        params.li = '\n';
    }else{
        params.li = '<br>';
    }
}

function sendWithDelayMsg(msg, delay) {
    var sendDelay = new java.lang.Runnable({
       run: function() {
           params.bot.sendFromBot(params.sessionId, msg);
       }
    });
    return scheduler.schedule(sendDelay, delay, java.util.concurrent.TimeUnit.MILLISECONDS);
}
